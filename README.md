# Description

This is the [mustache](https://mustache.github.io/mustache.5.html) template and the script I created for this website: [https://lunch.uppsala.ai](https://lunch.uppsala.ai).
It extract all of the lunch menus in Uppsala and outputs the lunch of today in a neat website.

# Installation
1) Download this folder and cd in it.
create local environement: 
2) `python3 -m venv lunch_pack`
3) `source lunch_pack/bin/activate`

4) `pip install -r requirements.txt`

# Usage:
1) cd to this folder
2) source `lunch_pack/bin/activate`
3) sh `render.sh`
4) Start modifying either base.html (structure) either base.yml (data)
5) in order to keep the website upgrading while modifying do:
`while true ;do sh render.sh ; sleep 10; done `

# Author:
Gitlab:
@mardub
Github:
@mardub1635
Website:
[http://www.uppsala.ai](http://www.uppsala.ai)
e-mail:
mardubr-lunch@yahoo.com