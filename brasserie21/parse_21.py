from bs4 import BeautifulSoup 
import urllib.request
import sys
from datetime import date
import re
import yaml

url='https://www.brasserie21.se/page/126571'
req = urllib.request.Request(
    url, 
    data=None, 
    headers={
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    }
)
response = urllib.request.urlopen(req)
bs= BeautifulSoup(response, "html.parser")

page_lines = bs.find_all(['p'])
#print(page_lines)
today = date.today()  
weekday=today.weekday()


menu_of_day=page_lines[weekday+2]
menu_of_day=menu_of_day.find_all(['strong'])

def unspace(text):
    """
    (String)->String
    given $text returns a copy without any useless white space characters.
    Example:
    >>>' Conference\nin machine translation:\xa0\xa0\xa0\xa0 a very difficult         problem. "
    'Conference in machine translation: a very difficult problem.
    """
    text = text.replace(u'\xa0', u' ').replace("\n\n", "\n").replace("\r", " ")
    text=text.replace("<br/>","")
    text = re.sub('<[a-z/]{1,10}>', ' ', text)
    text = re.sub(' +', ' ', text)
    text = text.strip()
    text = text.strip()

    return text

i=0
i_upper_lines=0

#Getting the vegetarisk of the week
vegetarisk=page_lines[1]
food=unspace(str(vegetarisk))
data0=['Soppa']

foods={"food":food}
data0.append(foods)
#Getting the food for the day
data1=['Kött']
print(menu_of_day[0])
food=unspace(str(menu_of_day[0]))
foods={"food":food}
data1.append(foods)
data2=['Fisk']
try:
    print(menu_of_day[1])
    food=unspace(str(menu_of_day[1]))
    foods={"food":food}
    data2.append(foods)
except:
    pass
try:
    data3=['Vegetarisk']
    print(menu_of_day[2])
    food=unspace(str(menu_of_day[2]))
    foods={"food":food}
    data3.append(foods)

except:
    pass
    
try:
	print(data0)
	#data0['food'] = list(dict.fromkeys(data0['food']))
except:
	data0=['Soppa', {'food': ['Nothing to show today']}]
try:
	print(data1)
except:
	data1=['Kött', {'food': ['Nothing to show today']}]
try:
	print(data2)
except:
	data2=['Fisk', {'food': ['Nothing to show today']}]
try:
    print(data3)
except:
    data3=['Vegetarisk', {'food': ['Nothing to show today']}]
data=data0+data1+data2+data3
print(data)

with open('brasserie21_menu.yml', 'w') as outfile:
    yaml.dump(data, outfile, default_flow_style=False)