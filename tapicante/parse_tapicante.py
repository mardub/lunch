from bs4 import BeautifulSoup 
import urllib.request
import sys
from datetime import date
import re
import yaml

url = "https://tapicante.com/acerca-de/"
response = urllib.request.urlopen(url)
bs= BeautifulSoup(response, "html.parser")

page_lines = bs.find_all(['p'])
print(page_lines)

today = date.today()  
weekday=today.weekday()
weekday_names=["Måndag", "Tisdag", "Onsdag", "Torsdag", "Fredag", "Lördag", "Söndag"]
today_name=weekday_names[weekday]
print(today_name)
tomorrow_name=weekday_names[weekday+1]
#menu_of_day=page_lines[weekday].parent.next_sibling
def unspace(text):
    """
    (String)->String
    given $text returns a copy without any useless white space characters.
    Example:
    >>>' Conference\nin machine translation:\xa0\xa0\xa0\xa0 a very difficult         problem. "
    'Conference in machine translation: a very difficult problem.
    """
    text = text.replace(u'\xa0', u' ').replace("\n\n", "\n").replace("\r", " ")
    text=text.replace("<br/>","")
    text = re.sub(' +', ' ', text)
    text = text.strip()
    #text = text.strip("–")
    text = text.strip()

    return text
data={}
data['food']=[]
stop=False
in_today=False
for line in page_lines:
	line=unspace(str(line.text))
	if stop:
		pass
	else:
		if tomorrow_name in line:
			in_today=False
			stop=True
			
		if in_today:
			data['food']+=[line]
			
		if today_name in line:
			in_today=True
			
		else:
			pass



with open('tapicante_menu.yml', 'w') as outfile:
    yaml.dump(data, outfile, default_flow_style=False)