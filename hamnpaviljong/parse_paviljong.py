from bs4 import BeautifulSoup 
import urllib.request
import sys
from datetime import date
import re
import yaml

url='http://hamnpaviljongen.com/lunchmeny/'
req = urllib.request.Request(
    url, 
    data=None, 
    headers={
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    }
)
response = urllib.request.urlopen(req)
bs= BeautifulSoup(response, "html.parser")

page_lines = bs.find_all(['h2'])
today = date.today()  
weekday=today.weekday()
menu_of_day=page_lines[weekday].next_sibling


def unspace(text):
    """
    (String)->String
    given $text returns a copy without any useless white space characters.
    Example:
    >>>' Conference\nin machine translation:\xa0\xa0\xa0\xa0 a very difficult         problem. "
    'Conference in machine translation: a very difficult problem.
    """
    text = text.replace(u'\xa0', u' ').replace("\n\n", "\n").replace("\r", " ")
    text=text.replace("<br/>","")
    text = re.sub(' +', ' ', text)
    text = text.strip()
    text = text.strip()

    return text

i=0
i_upper_lines=0
data0=['Menu']
menu=""
for line in menu_of_day:
	
	line=unspace(str(line))
	menu+="\n"+line
foods={"food":menu}
data0.append(foods)

# for line in menu_of_day:
	
# 		print(line)
# 		i+=1
# 		line=unspace(str(line))
# 		if "=" in line:
# 			break
# 		if "VEGETARISKA" in str(line) or "s veg" in line.lower():
# 			data0=['Vegetarisk']
# 			foods={"food":str(line).split(":")[1]}
# 			data0.append(foods)

# 		elif line=="":
# 			pass
# 		elif line.isupper() and i_upper_lines==0:
# 			data1=['Kott']
# 			foods={}
# 			foods['food']=[line]
# 			data1.append(foods)
# 			i_upper_lines+=1
# 			print("CA KOOOOTTT",data1,"CA KOOOOTTT")
# 		elif i_upper_lines==1 and not line.isupper():
# 			foods['food']+=[line]
# 		elif i_upper_lines==1 and line.isupper():
# 			i_upper_lines=2
# 			data2=['Fisk']
# 			foods2={}
# 			foods2['food']=[line]
# 		elif i_upper_lines==2 and not line.isupper():
# 			foods2['food']+=[line]
# 			data2.append(foods2)


# print(i)

try:
	print(data0)
	#data0['food'] = list(dict.fromkeys(data0['food']))
except:
	data0=['Vegetarisk', {'food': ['Nothing to show today']}]
# try:
# 	print(data1)
# except:
# 	data1=['Kött', {'food': ['Nothing to show today']}]
# try:
# 	print(data2)
# 	data2[1]['food']=list(dict.fromkeys(data2[1]['food']))

# except:
# 	data2=['Fisk', {'food': ['Nothing to show today']}]
data=data0#+data1+data2


with open('hamnpaviljong_menu.yml', 'w') as outfile:
    yaml.dump(data, outfile, default_flow_style=False)