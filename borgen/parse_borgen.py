from bs4 import BeautifulSoup 
import urllib.request
import sys
from datetime import date
from datetime import datetime
from PIL import Image 
import cv2
import numpy as np
import pytesseract
import re
import yaml
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

# Downloading the weekly menu image from borgen
url = "https://www.borgenuppsala.se/"
response = urllib.request.urlopen(url)
bs= BeautifulSoup(response, "html.parser")

noscript = bs.find_all("noscript")[8]
img=noscript.find("img")
if img:
    img_url = img['src']

import requests

with open('./borgen/menu.jpg', 'wb') as handle:
        response = requests.get(img_url, stream=True)

        if not response.ok:
            print(response)

        for block in response.iter_content(1024):
            if not block:
                break

            handle.write(block)

#Parsing the image
#using the tutorial: https://pythonprogramming.net/template-matching-python-opencv-tutorial/
menuColor=cv2.imread("./borgen/menu.jpg")


menu = cv2.cvtColor(menuColor, cv2.COLOR_BGR2GRAY)
template = cv2.imread('./borgen/croix.png',0)

w, h = template.shape[::-1]
res = cv2.matchTemplate(menu,template,cv2.TM_CCOEFF_NORMED)
threshold = 0.8
loc = np.where( res >= threshold)
#Removeme below: To show the result on the image 'detected1.jpg:
#for pt in zip(*loc[::-1]):
#    cv4=cv2.rectangle(menu, pt, (pt[0] + w, pt[1] + h), (0,255,255), 2)
#cv2.imwrite('borgen/detected1.jpg', cv4)

#
vertical=0
verticals=[]
horizontal=0
horizontals=[]
for i in range(len(loc[0])):
	if loc[0][i]>vertical+10:
		vertical=loc[0][i]
		verticals.append(vertical)
		horizontals.append(loc[1][i])

loc=(np.asarray(verticals),np.asarray(horizontals))

#fixe the wideness of the table
w=660 

# pick the right day of the week
today = date.today()  

weekday=today.weekday()
#REMOVEME ! Change the day below for test purpose
print("the time now is:***",datetime.now())
#weekday=1
#REMOVEME ! Change the day above for test purpose
#Found the coordinates, now lets extract the menus for each day
coordinates1=zip(*loc[::-1])
coordinates2=zip(*loc[::-1])
next(coordinates2)
i=0
for pts in zip(coordinates1,coordinates2):
    
    pt=pts[0]
    next_pt=pts[1]
    h=next_pt[1]-pt[1]
    if i==weekday:
        hday=next_pt[1]-pt[1]
        ptday=pts[0]
        next_ptday=pts[1]
    i+=1
    cv3=cv2.rectangle(menu, pt, (pt[0] +w , pt[1]+h ), (0,255,255), 2)
vegetarian_pt= next_pt
h=200
cv3=cv2.rectangle(menu, vegetarian_pt, (vegetarian_pt[0] +w , pt[1]+h ), (0,255,255), 2)

#for i in range(len(verticals)):
#	pt=(verticals[i],horizontals[i])
#	cv3=cv2.rectangle(menu, pt, (verticals[i] +w , horizontals[i]+h ), (0,255,255), 2)

cv2.imwrite('./borgen/Detected.jpg',cv3)
img=menuColor

vertical_start=ptday[1]
vertical_end=vertical_start+hday
horizontal_start=400#The position on the right of the day (after the letter "g" of måndag)
menu_width=1250
horizontal_end=horizontal_start+menu_width
crop_img = img[vertical_start:vertical_end,horizontal_start:horizontal_end]
#crop_img = img[vegetarian_pt[0]:300, pt[1]:33]
cv2.imwrite("./borgen/today_borgen.jpg", crop_img)

#Crop the vegetarian menu
hveg=200
crop_img = img[vegetarian_pt[1]:vegetarian_pt[1]+hveg, horizontal_start:horizontal_end]
cv2.imwrite("./borgen/today_borgen_vegetarian.jpg", crop_img)

#crop fisk
vertical_start=ptday[1]
vertical_end=vertical_start+hday
#horizontal_start=160

fisk_width=menu_width // 2 #Fisk Kott and vegetarian have the same width
horizontal_end=horizontal_start + fisk_width
crop_img = img[vertical_start:vertical_end,horizontal_start:horizontal_end]
cv2.imwrite("./borgen/today_borgen_fisk.jpg", crop_img)

#crop kott
vertical_start=ptday[1]
vertical_end=vertical_start+hday
horizontal_start=horizontal_start+fisk_width
horizontal_end=horizontal_start+fisk_width
crop_img = img[vertical_start:vertical_end,horizontal_start:horizontal_end]
cv2.imwrite("./borgen/today_borgen_kott.jpg", crop_img)



def unspace(text):
    """
    (String)->String
    given $text returns a copy without any useless white space characters.
    Example:
    >>>' Conference\nin machine translation:\xa0\xa0\xa0\xa0 a very difficult         problem. "
    'Conference in machine translation: a very difficult problem.
    """
    text = text.replace(u'\xa0', u' ').replace("\n\n", "\n").replace("\r", " ")
    text = re.sub(' +', ' ', text)
    text = text.strip()
    #text = text.strip("–")
    text = text.strip()
    return text


def clean(food):
    cleaned_food=list(filter(None, food))
    return cleaned_food

def dataifier(image_name,food_type):
    """
    (String, String)->[list,{dict}]
    given $image_name, $food_type ocr an image cleans it and returns structured list of menu element in a dictionary with
    its associated foodtype
    Example:
    >>>' Svamp Toast\nLingon, \r örtkräm, \xa0\xa0\xa0\xa0 dill.', 'Vegetarisk'
    '['Vegetarisk', {'Svamp Toast','Lingon, örtkräm, dill.'}]
    """
    image_string=pytesseract.image_to_string(Image.open(image_name),lang='swe')
    food_string=unspace(image_string)
    clean_food=clean(food_string.split("\n"))
    datum=[food_type]
    foods={}
    foods['food']=[]
    for food in clean_food:
        foods['food']+=[food]
    datum.append(foods)
    return datum


vegetarian=pytesseract.image_to_string(Image.open('./borgen/today_borgen_vegetarian.jpg'),lang='swe')
data0=dataifier('./borgen/today_borgen_vegetarian.jpg','Vegetarisk')

fisk=pytesseract.image_to_string(Image.open('./borgen/today_borgen_fisk.jpg'),lang='swe')
fisk=unspace(fisk)
clean_fisk=clean(fisk.split("\n"))
data1=['Fisk']
foods={}
foods['food']=[]
for food in clean_fisk:
    foods['food']+=[food]
data1.append(foods)
data1=dataifier('./borgen/today_borgen_fisk.jpg','Fisk')

kott=pytesseract.image_to_string(Image.open('./borgen/today_borgen_kott.jpg'),lang='swe')
kott=unspace(kott)
clean_kott=clean(kott.split("\n"))
data2=['Kott']
foods={}
foods['food']=[]
for food in clean_kott:
    foods['food']+=[food]
data2.append(foods)
data2=dataifier('./borgen/today_borgen_kott.jpg','Kött')
data=data0+data1+data2
print(data)
with open('borgen_menu.yml', 'w') as outfile:
    yaml.dump(data, outfile, default_flow_style=False)

