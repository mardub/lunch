import yaml
from yamlinclude import YamlIncludeConstructor

YamlIncludeConstructor.add_to_loader_class(loader_class=yaml.FullLoader, base_dir='.')

with open('base.yml') as f:
    data = yaml.load(f, Loader=yaml.FullLoader)
with open(r'index.yml', 'w') as file:
    documents = yaml.dump(data, file)
print(data)