from bs4 import BeautifulSoup 
import urllib.request
from datetime import date
import yaml
import PyPDF2

url='https://www.uppsalasaluhall.se/lunch'
req = urllib.request.Request(
    url, 
    data=None, 
    headers={
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    }
)
response = urllib.request.urlopen(req)
bs= BeautifulSoup(response, "html.parser")

page_lines = bs.find_all('a',attrs={'class':'food-menu__link'})
pdf=page_lines[0]['href']#link to the pdf
urllib.request.urlretrieve(pdf, "saluhallen_menu.pdf")
pdfFileObj = open('saluhallen_menu.pdf', 'rb')
# creating a pdf reader object
pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
pageObj = pdfReader.getPage(0)
menu=pageObj.extractText()
# closing the pdf file object
pdfFileObj.close()
def dataifier(food_list,food_type):

	datum=[food_type]
	foods={}
	foods['food']=food_list
	datum.append(foods)
	return datum

today = date.today()  
weekday=today.weekday()
weekday_names=["Måndag", "Tisdag", "Onsdag", "Torsdag", "Fredag", "Lördag", "Söndag"]
today_name=weekday_names[weekday]
tomorrow_name=weekday_names[weekday+1]
in_today=False
fisk=['Fisk']
gron=['Vegetarisk']
week_menu=['Kött']
kott=['Kött']
i=0
stop=False
#Each time we change food category the word Måndag is written, let's use it as a splitter
for line in menu.splitlines():
	#While not mondag yet
	if 'åndag' in line:
		i+=1
		continue
	elif 'veckans' in line:
		pass
	elif i==1:
		fisk.append(line)
	elif i==2:
		gron.append(line)
	elif i==3:
		week_menu.append(line)
		if today_name in line:
			in_today=True
			continue
		if stop:
			pass
		if tomorrow_name in line:
			in_today=False
			stop=True
		if in_today:
			kott.append(line)

data0=dataifier(fisk,'Fisk')
data1=dataifier(gron,'Vegetarisk')
data2=dataifier(kott,'Kött')
	
try:
	print(data0)
except:
	data0=['Vegetarisk', {'food': ['Nothing to show today']}]
try:
	print(data1)
except:
	data1=['Kött', {'food': ['Nothing to show today']}]
try:
	print(data2)
except:
	data2=['Fisk', {'food': ['Nothing to show today']}]
data=data0+data1+data2

with open('saluhallen_menu.yml', 'w') as outfile:
    yaml.dump(data, outfile, default_flow_style=False)