from datetime import datetime
import yaml
import locale
import pytz
today = datetime.now(tz=pytz.timezone('Europe/Paris'))  
print(today)
#locale.setlocale(locale.LC_TIME, "sv_SE") # swedish
today=today.strftime("%A, %d %B %Y %H:%M:%S")
print(today)
with open('today.yml', 'w') as outfile:
	yaml.dump(today, outfile, default_flow_style=False)